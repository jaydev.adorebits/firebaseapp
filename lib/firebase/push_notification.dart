import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class PushNotify extends StatefulWidget {
  @override
  PushNotifyState createState() => PushNotifyState();
}

class PushNotifyState extends State<PushNotify> {
  String _message = "";
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  _register() {
    _firebaseMessaging.getToken().then((token) => print('token: ' + token));
  }

  @override
  void initState() {
    super.initState();
    //getMessage();
    _register();
  }

  void getMessage() {
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
      print('on message $message');
      setState(() {
        _message = message["notification"]["title"];
      });
    }, onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
      setState(() {
        _message = message["notification"]["title"];
      });
    }, onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
      setState(() {
        _message = message["notification"]["title"];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Message : $_message'),
              OutlineButton(
                child: Text('Register My Device'),
                onPressed: () {
                  _register();
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
