import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

class User {
  String _id;
  String _uid;
  String _userName;
  String _userEmail;
  String _address;
  String _latitude;
  String _longitude;
  String _imageUrl;
  String _friendStatus;
  String _friendId;
  String _token;

  User(
      this._id,
      this._uid,
      this._userName,
      this._userEmail,
      this._address,
      this._latitude,
      this._longitude,
      this._imageUrl,
      this._friendStatus,
      this._friendId,
      this._token);

  String get id => _id;

  String get uid => _uid;

  String get userName => _userName;

  String get userEmail => _userEmail;

  String get address => _address;

  String get latitude => _latitude;

  String get longitude => _longitude;

  String get imageUrl => _imageUrl;

  String get friendStatus => _friendStatus;

  String get friendId => _friendId;

  String get token => _token;

  User.fromSnapShot(DataSnapshot snapShot) {
    _id = snapShot.key;
    _uid = snapShot.value['uid'];
    _userName = snapShot.value['userName'];
    _userEmail = snapShot.value['userEmail'];
    _address = snapShot.value['address'];
    _latitude = snapShot.value['latitude'];
    _longitude = snapShot.value['longitude'];
    _imageUrl = snapShot.value['imageUrl'];
    _friendStatus = snapShot.value['friendStatus'];
    _friendId = snapShot.value['friendId'];
    _token = snapShot.value['token'];
  }
}
