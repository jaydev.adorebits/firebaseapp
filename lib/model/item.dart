import 'package:firebase_database/firebase_database.dart';

class Item {
  String key;
  String userEmail;
  String userName;
  String address;
  String latitude;
  String longitude;
  String imageUrl;

  Item(this.userName, this.userEmail, this.address, this.latitude,
      this.longitude, this.imageUrl);

  Item.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        userName = snapshot.value["userName"],
        userEmail = snapshot.value["userEmail"],
        address = snapshot.value["address"],
        latitude = snapshot.value["latitude"],
        longitude = snapshot.value["longitude"],
        imageUrl = snapshot.value["imageUrl"];

  toJson() {
    return {
      "userName": userName,
      "userEmail": userEmail,
      "address": address,
      "latitude": latitude,
      "longitude": longitude,
      "imageUrl": imageUrl,
    };
  }
}
