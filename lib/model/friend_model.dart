import 'package:firebase_database/firebase_database.dart';

class Friend {
  String id;
  String _uid;
  String _userName;
  String _userEmail;
  String _address;
  String _latitude;
  String _longitude;
  String _imageUrl;
  String _senderId;
  String _receiverId;
  String _status;

  Friend(this._senderId, this._receiverId, this._status);

  /*String get uid => _uid;

  String get userName => _userName;

  String get userEmail => _userEmail;*/

  String get senderId => _senderId;

  String get receiverId => _receiverId;

  String get status => _status;

  Friend.fromSnapShot(DataSnapshot snapShot) {
    id = snapShot.key;
    _senderId = snapShot.value['senderId'];
    _receiverId = snapShot.value['receiverId'];
    _status = snapShot.value['status'];
  }
}

/*
Friend.fromSnapShot(DataSnapshot snapShot) {
    id = snapShot.key;
    _uid = snapShot.value['uid'];
    _userName = snapShot.value['userName'];
    _userEmail = snapShot.value['userEmail'];
    _senderId = snapShot.value['senderId'];
    _receiverId = snapShot.value['receiverId'];
    _status = snapShot.value['status'];
  }
 */