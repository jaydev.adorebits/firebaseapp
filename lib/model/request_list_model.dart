import 'package:firebase_database/firebase_database.dart';

class RequestListModel {
  String _id;
  String _uid;
  String _userName;
  String _userEmail;
  String _address;
  String _latitude;
  String _longitude;
  String _imageUrl;
  String _senderId;
  String _receiverId;
  String _status;
  String _token;

  RequestListModel(
      this._id,
      this._uid,
      this._userName,
      this._userEmail,
      this._address,
      this._latitude,
      this._longitude,
      this._imageUrl,
      this._senderId,
      this._receiverId,
      this._status,
      this._token);

  String get id => _id;

  String get uid => _uid;

  String get userName => _userName;

  String get userEmail => _userEmail;

  String get address => _address;

  String get latitude => _latitude;

  String get longitude => _longitude;

  String get imageUrl => _imageUrl;

  String get senderId => _senderId;

  String get receiverId => _receiverId;

  String get status => _status;

  String get token => _token;

  RequestListModel.fromSnapShot(DataSnapshot snapShot) {
    _id = snapShot.key;
    _uid = snapShot.value['uid'];
    _userName = snapShot.value['userName'];
    _userEmail = snapShot.value['userEmail'];
    _address = snapShot.value['address'];
    _latitude = snapShot.value['latitude'];
    _longitude = snapShot.value['longitude'];
    _imageUrl = snapShot.value['imageUrl'];
    _token = snapShot.value['token'];
  }
}
