import 'package:firebase_app/screens/friend_list.dart';
import 'package:firebase_app/screens/near_user.dart';
import 'package:firebase_app/screens/request_list.dart';
import 'package:flutter/material.dart';
import 'home_screen.dart';

class BottomTab extends StatefulWidget {
  String userName;
  String userEmail;
  String uid;

  BottomTab({Key key, this.userName, this.userEmail, this.uid});

  @override
  BottomTabState createState() => BottomTabState();
}

class BottomTabState extends State<BottomTab> {
  BottomTab tab = new BottomTab();

  int currentTab = 0;
  Widget currentScreen;
  List<Widget> screens = List();
  final PageStorageBucket bucket = PageStorageBucket();

  @override
  void initState() {
    super.initState();

    screens.add(HomeScreen(
      userName: widget.userName,
      userEmail: widget.userEmail,
      uid: widget.uid,
    ));
    screens.add(FriendList());
    screens.add(RequestList());
    currentScreen = HomeScreen(
        userName: widget.userName,
        userEmail: widget.userEmail,
        uid: widget.uid);

    // to keep track of active tab index
    /* List<Widget> screens = [HomeScreen(), NearUser()]; // to store nested tabs
    final PageStorageBucket bucket = PageStorageBucket();
    Widget currentScreen = HomeScreen(
        userName: tab.userName, userEmail: ""); // Our first view in viewport*/
  }

  @override
  Widget build(Object context) {
    return Scaffold(
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly, //
                // crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  MaterialButton(
                    onPressed: () {
                      setState(() {
                        currentScreen = HomeScreen(
                            userName: widget.userName,
                            userEmail: widget.userEmail,
                            uid: widget.uid);
                        currentTab = 0;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.people,
                          color: currentTab == 0 ? Colors.blue : Colors.grey,
                        ),
                        Text(
                          'Users',
                          style: TextStyle(
                            color: currentTab == 0 ? Colors.blue : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  MaterialButton(
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            FriendList(userId: widget.uid,); // if user taps on this dashboard tab will be active
                        currentTab = 1;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.near_me,
                          color: currentTab == 1 ? Colors.blue : Colors.grey,
                        ),
                        Text(
                          'Friends',
                          style: TextStyle(
                            color: currentTab == 1 ? Colors.blue : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  MaterialButton(
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            RequestList(userId: widget.uid); // if user taps on this dashboard tab will be active
                        currentTab = 2;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.person_add,
                          color: currentTab == 2 ? Colors.blue : Colors.grey,
                        ),
                        Text(
                          'Request List',
                          style: TextStyle(
                            color: currentTab == 2 ? Colors.blue : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),

              // Right Tab bar icons
            ],
          ),
        ),
      ),
    );
  }
}
