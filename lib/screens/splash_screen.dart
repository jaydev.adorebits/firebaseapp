import 'dart:async';

import 'package:firebase_app/main.dart';
import 'package:firebase_app/screens/bottom_tab.dart';
import 'package:firebase_app/screens/register_user_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import 'auth_screen.dart';
import 'home_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  _getFirebaseToken() {
    _firebaseMessaging.getToken().then((token) => print('token_s : ' + token));
  }

  @override
  void initState() {
    super.initState();
    navigateUser();
    _getFirebaseToken();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/bg.png"),
                    fit: BoxFit.cover)),
          ),
        ],
      ),
    );
  }

  navigateUser() {
    FirebaseAuth.instance.currentUser().then((currentUser) {
      if (currentUser == null)
      {
        Timer(Duration(seconds: 2),
            () => Navigator.pushReplacementNamed(context, '/auth'));
      } else {
        print('uid ' + currentUser.uid + " kk");
        Timer(
            Duration(seconds: 3),
            () => FirebaseDatabase.instance
                    .reference()
                    .child("user")
                    .orderByChild("uid")
                    .equalTo(currentUser.uid)
                    .once()
                    .then((DataSnapshot snapshot) {
                  if (snapshot != null && snapshot.value != null) {
                    print('not null getting value');
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (context) => BottomTab(
                                  userName: currentUser.displayName,
                                  userEmail: currentUser.email,
                                  uid: currentUser.uid,
                                )),
                        (Route<dynamic> route) => false);
                  } else {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (context) => RegisterUser(
                                  uid: currentUser.uid,
                                  username: currentUser.displayName,
                                  emailAddress: currentUser.email,
                                )),
                        (Route<dynamic> route) => false);
                  }
                }));

        /*Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                  builder: (context) => RegisterUser(
                        uid: currentUser.uid,
                        username: currentUser.displayName,
                        emailAddress: currentUser.email,
                      ) */ /*HomeScreen(userName: currentUser.displayName, userEmail: currentUser.email,)*/ /*),
              (Route<dynamic> route) => false),*/

      }
    });
  }
}
