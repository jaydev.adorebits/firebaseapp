import 'package:firebase_app/firebase/firebase_utils_class.dart';
import 'package:firebase_app/model/user.dart';
import 'package:firebase_app/screens/auth_screen.dart';
import 'package:firebase_app/screens/profile_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  String userName;
  String userEmail;
  String uid;

  HomeScreen({Key key, this.userName, this.userEmail, this.uid});

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  bool _anchorToBottom = false;
  FirebaseDatabaseUtil databaseUtil;

  DatabaseReference _counterRef;
  DatabaseReference _userFriends;
  DatabaseReference db;
  FirebaseDatabase database = new FirebaseDatabase();

  FirebaseStorage storage = FirebaseStorage.instance;
  StorageReference reference;

  String _message = "";
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    print('home userName ' + widget.userName);
    print('home userEmail ' + widget.userEmail);
    databaseUtil = new FirebaseDatabaseUtil();
    databaseUtil.initState();

    _counterRef = FirebaseDatabase.instance.reference().child('counter');
    _userFriends = database.reference().child('friends');
    db = FirebaseDatabase.instance.reference().child("user");

    db.once().then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> values = snapshot.value;
      values.forEach((key, values) {
        print('key_users ' + values['userEmail'] + " " + key + " kk");
      });
    });

    _userFriends.once().then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> values = snapshot.value;
      values.forEach((key, values) {
        print('key_frnds ' +
            values['senderId'] +
            " " +
            key +
            " kk " +
            widget.uid);
        if (widget.uid == values['senderId'].toString()) {
          print('keyStatus else ' + values['status']);
        } else
          print('keyElse not found');
      });
    });

    getMessage();
  }

  @override
  void dispose() {
    super.dispose();
    databaseUtil.dispose();
  }

  void getMessage() {
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
      print('on message $message' + " home");

      setState(() {
        _message = message["notification"]["title"];
        print('on message msg1 ' + _message);
        print('on message body1 ' + message["notification"]["body"]);
        print('on message data ' + message["notification"]["body"]["type"]);
        print('on message data1 ' + message["notification"]["type"]);
        print('on message data2 ' +
            message["notification"]["body"]["type"]["name"]);
      });
    }, onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
      setState(() {
        _message = message["notification"]["title"];
      });
    }, onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
      setState(() {
        _message = message["notification"]["title"];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Hie ' + widget.userName),
          actions: [
            IconButton(
              icon: Icon(
                Icons.person,
                color: Colors.white,
              ),
              /*onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            ProfileScreen(uid: widget.uid)));
              },*/
            ),
            IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: () async {
                await FirebaseAuth.instance.signOut();
                await googleSignIn.disconnect();
                await googleSignIn.signOut();
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => AuthScreen()),
                    (Route<dynamic> route) => false);
              },
            )
          ],
        ),
        body: new FirebaseAnimatedList(
            key: new ValueKey<bool>(_anchorToBottom),
            query: databaseUtil.getUser(),
            sort: _anchorToBottom
                ? (DataSnapshot a, DataSnapshot b) => b.key.compareTo(a.key)
                : null,
            itemBuilder: (BuildContext context, DataSnapshot snapshot,
                Animation<double> animation, int index) {
              return new SizeTransition(
                sizeFactor: animation,
                child: showUser(snapshot),
              );
            }));
  }

  Widget showUser(DataSnapshot res) {
    User user = User.fromSnapShot(res);
    var item = new Card(
        elevation: 2,
        margin: EdgeInsets.fromLTRB(14, 6, 14, 4),
        child: Stack(
          children: [
            Container(
                margin: EdgeInsets.all(8),
                child: new Center(
                  child: Column(
                    children: [
                      new Row(
                        children: <Widget>[
                          Container(
                              height: 58.0,
                              width: 58.0,
                              child: user.imageUrl.isEmpty
                                  ? CircleAvatar(
                                      radius: 30.0,
                                      child:
                                          Text(user.userName.substring(0, 1)),
                                      backgroundColor: const Color(0xFF20283e),
                                    )
                                  : CircleAvatar(
                                      radius: 30,
                                      backgroundImage:
                                          NetworkImage(user.imageUrl),
                                    )),
                          new Expanded(
                            child: new Padding(
                              padding: EdgeInsets.all(18.0),
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Text(
                                    user.userName,
                                    // set some style to text
                                    style: new TextStyle(
                                        fontSize: 16.0, color: Colors.blue),
                                  ),
                                  new Text(
                                    user.userEmail,
                                    style: new TextStyle(
                                        fontSize: 16.0, color: Colors.blue),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      /*Row(
                  children: [
                    Container(
                      child: Text('Address : '),
                    ),
                    Expanded(
                      child: new Text(
                        user.address,
                        style:
                            new TextStyle(fontSize: 16.0, color: Colors.blue),
                      ),
                    )
                  ],
                )*/
                    ],
                  ),
                ),
                padding: const EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)),
            Align(
              alignment: Alignment.bottomRight,
              child: Column(
                children: [
                  Padding(
                      padding: EdgeInsets.only(right: 6),
                      child: widget.uid != user.uid
                          ? IconButton(
                              onPressed: () {
                                print('userUid ' + user.uid);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            ProfileScreen(
                                              uid: widget.uid,
                                              user: user,
                                            )));
                              },
                              icon: Icon(Icons.send, color: Colors.blue),
                            )
                          : Container()),
                ],
              ),
            )
          ],
        ));

    return item;
  }

  sendRequest(senderId, String receiverId, String status) async {
    final TransactionResult transactionResult =
        await _counterRef.runTransaction((MutableData mutableData) async {
      mutableData.value = (mutableData.value ?? 0) + 1;

      return mutableData;
    });

    if (transactionResult.committed) {
      _userFriends.push().set(<String, String>{
        "senderId": "" + senderId,
        "receiverId": "" + receiverId,
        "status": "" + status,
      }).then((_) {
        print('Transaction  committed.');
        print('frnd added');
        getSingleUser(senderId);
      });
    } else {
      print('Transaction not committed.');
      if (transactionResult.error != null) {
        print(transactionResult.error.message);
      }
    }
  }

  void getSingleUser(String senderKey) {
    try {
      db.once().then((DataSnapshot snapshot) {
        String id;
        String userName;
        String userEmail;
        String address;
        String latitude;
        String longitude;
        String imageUrl;
        String uid;

        if (snapshot != null && snapshot.value != null) {
          Map<dynamic, dynamic> values = snapshot.value;
          values.forEach((key, values) {
            print('key ' + key + " " + senderKey + " sender");
            if (key == senderKey) {
              userName = values['userName'].toString();
              userEmail = values['userEmail'].toString();
              address = values['address'].toString();
              latitude = values['latitude'].toString();
              longitude = values['longitude'].toString();
              imageUrl = values['imageUrl'].toString();
              uid = values['uid'].toString();

              print('userKS ' + values['userName'].toString() + " values  kk");
              print('userKS ' + values['userEmail'].toString() + " values  kk");
              print('userKS ' + values['address'].toString() + " values  kk");
              print('userKS ' + values['latitude'].toString() + " values  kk");
              print('userKS ' + values['longitude'].toString() + " values  kk");
              print('userKS ' + values['imageUrl'].toString() + " values  kk");
              print('userKS ' + values['uid'].toString() + " values  kk");
              print('userKS ' +
                  values['friendStatus'].toString() +
                  " values  kk");
              print('userKS ' + values['friendId'].toString() + " values  kk");
            } else
              print('userKeeyyElse ' +
                  values['userName'].toString() +
                  " values  kk");
          });
        } else
          print('else ');
      });
    } catch (e) {
      print("exe exe" + e.toString());
    }
  }
}

/*
sendRequest(String uid, String senderId, String receiverId, String userName,
      String userEmail, String status) async {
    final TransactionResult transactionResult =
        await _counterRef.runTransaction((MutableData mutableData) async {
      mutableData.value = (mutableData.value ?? 0) + 1;

      return mutableData;
    });

    if (transactionResult.committed) {
      _userFriends.push().set(<String, String>{
        "uid": "" + uid,
        "userName": "" + userName,
        "userEmail": "" + userEmail,
        "senderId": "" + senderId,
        "receiverId": "" + receiverId,
        "status": "" + status,
      }).then((_) {
        print('Transaction  committed.');
        print('frnd added');
      });
    } else {
      print('Transaction not committed.');
      if (transactionResult.error != null) {
        print(transactionResult.error.message);
      }
    }
  }
 */
/*
Padding(
                      padding: EdgeInsets.only(right: 6),
                      child: widget.uid != user.uid
                          ? user.friendStatus == 'send'
                              ? IconButton(
                                  onPressed: () {
                                    print('userUid ' + user.uid);
                                    sendRequest(
                                        widget.uid, user.uid, "pending");
                                  },
                                  icon: Icon(Icons.send, color: Colors.blue),
                                )
                              : Container(
                                  child: user.friendStatus != 'friend'
                                      ? IconButton(
                                          onPressed: () {
                                            print('userUid ' + user.uid);
                                          },
                                          icon: Icon(Icons.send,
                                              color: Colors.red),
                                        )
                                      : Container())
                          : Container()),
 */
