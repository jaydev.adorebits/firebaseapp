import 'package:connectivity/connectivity.dart';
import 'package:firebase_app/constant/Constant.dart';
import 'package:firebase_app/model/request_list_model.dart';
import 'package:firebase_app/model/user.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class RequestList extends StatefulWidget {
  String userId;

  RequestList({Key key, this.userId});

  @override
  RequestListState createState() => RequestListState();
}

class RequestListState extends State<RequestList> {
  DatabaseReference _counterRef;
  DatabaseReference _userFriends;
  DatabaseReference _userRef;

  List<RequestListModel> users = List();
  bool flag = false;
  bool isVisible = true;

  @override
  void initState() {
    super.initState();
    users = List();

    print('userId ' + widget.userId);

    check().then((intenet) {
      if (intenet != null && intenet) {
        print('net is available ');
        FirebaseDatabase database = FirebaseDatabase.instance;
        _counterRef = FirebaseDatabase.instance.reference().child('counter');
        _userFriends = database.reference().child('friends');
        _userRef = database.reference().child('user');

        getRequestedUsers();
      } else {
        Fluttertoast.showToast(msg: 'Please check your internet connection');
      }
      // No-Internet Case
    });
  }

  void getSingleUser(String senderKey, String updateKey, String senderId,
      String receiverId, String status) {
    try {
      _userRef.once().then((DataSnapshot snapshot) {
        if (snapshot != null && snapshot.value != null) {
          Map<dynamic, dynamic> values = snapshot.value;
          values.forEach((key, values) {
            print('key ' + key + " " + senderKey + " sender");

            if (key == senderKey) {
              String id = updateKey;
              String userName = values['userName'].toString();
              String userEmail = values['userEmail'].toString();
              String address = values['address'].toString();
              String latitude = values['latitude'].toString();
              String longitude = values['longitude'].toString();
              String imageUrl = values['imageUrl'].toString();
              String uid = values['uid'].toString();
              String token = values['token'].toString();
              print('userKS ' + values['userName'].toString() + " values  kk");
              print('userSize ' + users.length.toString() + " kk");
              setState(() {
                users.add(new RequestListModel(
                    id,
                    uid,
                    userName,
                    userEmail,
                    address,
                    latitude,
                    longitude,
                    imageUrl,
                    senderId,
                    receiverId,
                    status,
                    token));
                isVisible = false;
              });
            } else
              print('userKeeyyElse ' +
                  values['userName'].toString() +
                  " values  kk");
          });
        } else
          print('else ');
      });
    } catch (e) {
      print("exe exe" + e.toString());
    }
  }

  void getSingleReceiverList(String senderKey, String frndStatus) {
    try {
      _userRef.once().then((DataSnapshot snapshot) {
        if (snapshot != null && snapshot.value != null) {
          Map<dynamic, dynamic> values = snapshot.value;
          values.forEach((key, values) {
            print('key ' + key + " " + senderKey + " sender");
            if (key == senderKey) {
              String userName = values['userName'].toString();
              String userEmail = values['userEmail'].toString();
              String address = values['address'].toString();
              String latitude = values['latitude'].toString();
              String longitude = values['longitude'].toString();
              String imageUrl = values['imageUrl'].toString();
              String uid = values['uid'].toString();
              String token = values['token'].toString();

              print('userSender ' +
                  values['userName'].toString() +
                  " receiver  kk");
              User userUpdate = new User(
                  senderKey,
                  uid,
                  userName,
                  userEmail,
                  address,
                  latitude,
                  longitude,
                  imageUrl,
                  frndStatus,
                  "0",
                  token);
              registerUser(userUpdate);
            } else
              print('receiver ' + " values  kk");
          });
        } else
          print('else ');
      });
    } catch (e) {
      print("exe exe" + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Request List'),
      ),
      body: isVisible
          ? Center(
              child: Visibility(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Color(0xFFB2F2D52)),
                ),
                visible: isVisible,
              ),
            )
          : Container(
              child: users.length != 0
                  ? ListView.builder(
                      itemCount: users.length,
                      itemBuilder: (BuildContext context, int index) {
                        RequestListModel user = users[index];
                        return cardWidget(
                            users[index].imageUrl,
                            users[index].userName,
                            users[index].userEmail,
                            users[index].id,
                            user);
                      },
                    )
                  : Center(
                      child: Text('No request found'),
                    )),
    );
  }

  Widget cardWidget(String imageUrl, String userName, String userEmail,
      String id, RequestListModel user) {
    return Card(
      elevation: 2,
      margin: EdgeInsets.fromLTRB(14, 6, 14, 4),
      child: new Container(
          margin: EdgeInsets.all(8),
          child: new Center(
            child: Column(
              children: [
                new Row(
                  children: <Widget>[
                    Container(
                        height: 58.0,
                        width: 58.0,
                        child: imageUrl.isEmpty
                            ? CircleAvatar(
                                radius: 30.0,
                                child: Text(userName.substring(0, 1)),
                                backgroundColor: const Color(0xFF20283e),
                              )
                            : CircleAvatar(
                                radius: 30,
                                backgroundImage: NetworkImage(imageUrl),
                              )),
                    new Expanded(
                      child: new Padding(
                        padding: EdgeInsets.all(18.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text(
                              userName,
                              // set some style to text
                              style: new TextStyle(
                                  fontSize: 16.0, color: Colors.blue),
                            ),
                            new Text(
                              userEmail,
                              style: new TextStyle(
                                  fontSize: 16.0, color: Colors.blue),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4),
                      ),
                      onPressed: () {
                        print('updateKey :' + id + " kk");
                        setState(() {
                          isVisible = true;
                        });
                        updateRequest(user.senderId, user.receiverId,
                            "Accepted", id, user, "friend");
                        print('childKey ' +
                            id +
                            " accepted " +
                            user.status +
                            " " +
                            user.receiverId +
                            " " +
                            user.senderId);
                      },
                      padding: EdgeInsets.all(12),
                      color: Colors.green,
                      child: Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        child: Text(
                          'Accept',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                    RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4),
                      ),
                      onPressed: () {
                        setState(() {
                          isVisible = true;
                        });
                        updateRequest(user.senderId, user.receiverId,
                            "Rejected", id, user, "send");
                      },
                      padding: EdgeInsets.all(12),
                      color: Colors.red,
                      child: Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        child: Text(
                          'Cancel',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          padding: const EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)),
    );
  }

  updateRequest(senderId, String receiverId, String status, String key,
      RequestListModel user, String frndStatus) async {
    final TransactionResult transactionResult =
        await _counterRef.runTransaction((MutableData mutableData) async {
      mutableData.value = (mutableData.value ?? 0) + 1;

      return mutableData;
    });

    if (transactionResult.committed) {
      _userFriends.child(key).set(<String, String>{
        "senderId": "" + senderId,
        "receiverId": "" + receiverId,
        "status": "" + status,
      }).then((_) {
        updateSendRequest(receiverId, senderId, status);
        setState(() {
          print('Transaction update  committed.');
          User userUpdate = new User(
              senderId,
              senderId,
              user.userName,
              user.userEmail,
              user.address,
              user.latitude,
              user.longitude,
              user.imageUrl,
              frndStatus,
              "",
              user.token);
          setState(() {
            isVisible = false;
          });
          registerUser(userUpdate);
          //getSingleReceiverList(receiverId, frndStatus);
          getRequestedUsers();
        });
      });
    } else {
      print('Transaction not committed.');
      if (transactionResult.error != null) {
        print(transactionResult.error.message);
      }
    }
  }

  void getRequestedUsers() {
    users.clear();
    _userFriends
        .orderByChild('receiverId')
        .equalTo(widget.userId)
        .once()
        .then((DataSnapshot snapshot) {
      if (snapshot != null && snapshot.value != null) {
        Map<dynamic, dynamic> values = snapshot.value;
        values.forEach((key, values) {
          if (values['status'].toString() == 'pending') {
            String senderId = values['senderId'].toString();
            String receiverId = values['receiverId'].toString();
            String status = values['status'].toString();
            getSingleUser(values['senderId'].toString(), key, senderId,
                receiverId, status);
          } else {
            setState(() {
              isVisible = false;
            });
            print('loader 1');
            print('status accepted');
          }
        });
        print('userKeeyy ' + snapshot.key + " kk_else");
      } else {
        print('loader 2');
        print('userKeeyy ' + snapshot.key + " not kk_else");
      }
    });
  }

  registerUser(User user) async {
    final TransactionResult transactionResult =
        await _counterRef.runTransaction((MutableData mutableData) async {
      mutableData.value = (mutableData.value ?? 0) + 1;

      return mutableData;
    });

    if (transactionResult.committed) {
      _userRef.child(user.uid).set(<String, String>{
        "uid": "" + user.uid,
        "userName": "" + user.userName,
        "userEmail": "" + user.userEmail,
        "address": "" + user.address,
        "latitude": "" + user.latitude,
        "longitude": "" + user.longitude,
        "imageUrl": "" + user.imageUrl,
        "friendStatus": "" + user.friendStatus,
      }).then((_) {
        print('Transaction user updated.');
      });
    } else {
      print('Transaction not committed.');
      if (transactionResult.error != null) {
        print(transactionResult.error.message);
      }
    }
  }

  updateSendRequest(senderId, String receiverId, String status) async {
    final TransactionResult transactionResult =
        await _counterRef.runTransaction((MutableData mutableData) async {
      mutableData.value = (mutableData.value ?? 0) + 1;

      return mutableData;
    });

    if (transactionResult.committed) {
      _userFriends.push().set(<String, String>{
        "senderId": "" + senderId,
        "receiverId": "" + receiverId,
        "status": "" + status,
      }).then((_) {
        //print('Transaction  committed.');
        print('updateSendRequest updated ');
        //getRequestedUsers();
      });
    } else {
      print('Transaction not committed.');
      if (transactionResult.error != null) {
        print(transactionResult.error.message);
      }
    }
  }
}
