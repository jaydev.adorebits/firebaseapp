import 'dart:math' show cos, sqrt, asin;
import 'package:android_intent/android_intent.dart';
import 'package:firebase_app/firebase/firebase_utils_class.dart';
import 'package:firebase_app/model/item.dart';
import 'package:firebase_app/model/near_user.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

class NearUser extends StatefulWidget {
  @override
  NearUserState createState() => NearUserState();
}

class NearUserState extends State<NearUser> {
  bool _anchorToBottom = false;
  FirebaseDatabaseUtil databaseUtil;

  double currentLatitude = 0.0;
  double currentLongitude = 0.0;

  List<Item> Users = List();
  Item item;
  DatabaseReference itemRef;
  TextEditingController controller = new TextEditingController();
  static String filter;

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final PermissionHandler permissionHandler = PermissionHandler();
  Map<PermissionGroup, PermissionStatus> permissions;

  List<NearList> nearUsers = List();

  @override
  void initState() {
    super.initState();

    databaseUtil = new FirebaseDatabaseUtil();
    databaseUtil.initState();

    item = Item("", "", "", "", "", "");
    FirebaseDatabase database = FirebaseDatabase
        .instance; //Rather then just writing FirebaseDatabase(), get the instance.
    itemRef = database.reference().child('user');
    itemRef.onChildAdded.listen(_onEntryAdded);
    itemRef.onChildChanged.listen(_onEntryChanged);

    filter = controller.text;

    controller.addListener(() {
      setState(() {
        filter = controller.text;
        print('add controller');
      });
    });

    requestLocationPermission();
    _gpsService();

    itemRef.once().then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> values = snapshot.value;
      values.forEach((key, values) {
        nearUsers.add(new NearList(
            values['userName'], values['latitude'], values['longitude']));
        print('user ' + values['latitude'] + values['latitude'] + " kk");
      });
    });

    /*double totalDistance = 0;
    for(var i = 0; i < data.length-1; i++){
      totalDistance += calculateDistance(data[i]["lat"], data[i]["lng"], data[i+1]["lat"], data[i+1]["lng"]);
    }
    print(totalDistance);*/
  }

  @override
  void dispose() {
    super.dispose();
    databaseUtil.dispose();
  }

  _onEntryAdded(Event event) {
    setState(() {
      Users.add(Item.fromSnapshot(event.snapshot));
    });
  }

  _onEntryChanged(Event event) {
    var old = Users.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });
    setState(() {
      Users[Users.indexOf(old)] = Item.fromSnapshot(event.snapshot);
    });
  }

  void handleSubmit() {
    final FormState form = formKey.currentState;

    if (form.validate()) {
      form.save();
      form.reset();
      itemRef.push().set(item.toJson());
    }
  }

  Future<double> getDistance(double lat, double long) async {
    final distanceInMeters = await Geolocator()
        .distanceBetween(currentLatitude, currentLongitude, lat, long);

    return distanceInMeters / 5000;
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Near By You'),
          actions: [
            IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: () async {
                /*await FirebaseAuth.instance.signOut();
                await googleSignIn.disconnect();
                await googleSignIn.signOut();
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => AuthScreen()),
                    (Route<dynamic> route) => false);*/
                print('list ' + nearUsers.length.toString() + " kk");
                /*for (int i = 0; i < nearUsers.length; i++) {
                  print('list Name' + nearUsers[i].userName + " kk");
                }*/
                /*final queryLocation = GeoPoint(37.7853889, -122.4056973);

                // creates a new query around [37.7832, -122.4056] with a radius of 0.6 kilometers
                final List<DocumentSnapshot> documents = await geoFirestore.getAtLocation(queryLocation, 0.6);
                documents.forEach((document) {
                  print(document.data);
                });*/
              },
            )
          ],
        ),
        resizeToAvoidBottomPadding: false,
        body: Container(
          child: Column(
            children: [
              Container(
                height: 80,
                child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: TextField(
                    onChanged: (value) {},
                    decoration: InputDecoration(
                        labelText: 'Search',
                        hintText: 'Search Near User',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        ),
                        contentPadding:
                            EdgeInsets.only(left: 14, right: 14, top: 4)),
                    controller: controller,
                  ),
                ),
              ),
              Expanded(
                child: FirebaseAnimatedList(
                    //key: new ValueKey<bool>(_anchorToBottom),
                    query: itemRef,
                    itemBuilder: (BuildContext context, DataSnapshot snapshot,
                        Animation<double> animation, int index) {
                      return Users[index].userName.contains(filter)
                          ? new
                      Card(
                              elevation: 2,
                              margin: EdgeInsets.fromLTRB(14, 6, 14, 4),
                              child: new Container(
                                  margin: EdgeInsets.all(8),
                                  child: new Center(
                                    child: Column(
                                      children: [
                                        new Row(
                                          children: <Widget>[
                                            Container(
                                                height: 58.0,
                                                width: 58.0,
                                                child: Users[index]
                                                        .imageUrl
                                                        .isEmpty
                                                    ? CircleAvatar(
                                                        radius: 30.0,
                                                        child: Text(Users[index]
                                                            .userName
                                                            .substring(0, 1)),
                                                        backgroundColor:
                                                            const Color(
                                                                0xFF20283e),
                                                      )
                                                    : CircleAvatar(
                                                        radius: 30,
                                                        backgroundImage:
                                                            NetworkImage(
                                                                Users[index]
                                                                    .imageUrl),
                                                      )),
                                            new Expanded(
                                              child: new Padding(
                                                padding: EdgeInsets.all(18.0),
                                                child: new Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    new Text(
                                                      Users[index].userName,
                                                      // set some style to text
                                                      style: new TextStyle(
                                                          fontSize: 16.0,
                                                          color: Colors.blue),
                                                    ),
                                                    new Text(
                                                      Users[index].userEmail,
                                                      style: new TextStyle(
                                                          fontSize: 16.0,
                                                          color: Colors.blue),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  padding: const EdgeInsets.fromLTRB(
                                      10.0, 0.0, 0.0, 0.0)),
                            )
                          /*ListTile(
                              leading: Icon(Icons.person),
                              title: Text(Users[index].userName),
                              subtitle: Text(Users[index].userEmail),
                            )*/
                          : Container();
                    }),
              )
            ],
          ),
        ));
  }

  _getLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    debugPrint('location: ${position.latitude}');
    final coordinates = new Coordinates(position.latitude, position.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    print("${first.featureName} : ${first.addressLine}");
    setState(() {
      print("location :" +
          first.adminArea +
          " kk " +
          position.latitude.toString() +
          " k " +
          position.longitude.toString() +
          " ty");
    });
  }

  Future<bool> _requestPermission(PermissionGroup permission) async {
    final PermissionHandler _permissionHandler = PermissionHandler();
    var result = await _permissionHandler.requestPermissions([permission]);
    if (result[permission] == PermissionStatus.granted) {
      print('perm granted');
      return true;
    }
    return false;
  }

/*Checking if your App has been Given Permission*/
  Future<bool> requestLocationPermission({Function onPermissionDenied}) async {
    var granted = await _requestPermission(PermissionGroup.location);
    if (granted != true) {
      requestLocationPermission();
    }
    debugPrint('requestContactsPermission $granted');
    return granted;
  }

/*Show dialog if GPS not enabled and open settings location*/
  Future _checkGps() async {
    if (!(await Geolocator().isLocationServiceEnabled())) {
      if (Theme.of(context).platform == TargetPlatform.android) {
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Can't get current location"),
                content:
                    const Text('Please make sure you enable GPS and try again'),
                actions: <Widget>[
                  FlatButton(
                      child: Text('Ok'),
                      onPressed: () {
                        final AndroidIntent intent = AndroidIntent(
                            action:
                                'android.settings.LOCATION_SOURCE_SETTINGS');
                        intent.launch();
                        Navigator.of(context, rootNavigator: true).pop();
                        _gpsService();
                      })
                ],
              );
            });
      }
    }
  }

/*Check if gps service is enabled or not*/

  Future _gpsService() async {
    if (!(await Geolocator().isLocationServiceEnabled()))
    {
      _checkGps();
      return null;
    } else {
      _getLocation();
      print('perm granted gps 451');
      return true;
    }
  }
}

