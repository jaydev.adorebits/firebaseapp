import 'dart:async';
import 'dart:io';

import 'package:firebase_app/model/user.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';

class ProfileScreen extends StatefulWidget {
  String uid;
  User user;

  ProfileScreen({Key key, this.uid, this.user});

  @override
  ProfileScreenState createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {
  String _firstName = "";
  String _email = "";
  String _address = "";
  String _latitude = "";
  String _longitude = "";
  String uid = "";
  String imageLink = "";

  TextEditingController textUserName = TextEditingController();
  TextEditingController textEmailAddress = TextEditingController();
  TextEditingController textAddress = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  User user;
  String buttonName = "Send Request";

  bool isVisible = true;
  bool _anchorToBottom = false;

  //FirebaseDatabaseUtil databaseUtil;
  DatabaseReference _counterRef;
  DatabaseReference _userRef;
  DatabaseReference _userFriends;
  FirebaseDatabase database = new FirebaseDatabase();

  FirebaseStorage storage = FirebaseStorage.instance;
  StorageReference reference;

  File imageURI;

  Future getImageFromCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      imageURI = image;
    });
  }

  Future getImageFromGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      imageURI = image;
    });
  }

  @override
  void initState() {
    super.initState();
    _counterRef = FirebaseDatabase.instance.reference().child('counter');
    _userRef = database.reference().child('user');
    _userFriends = database.reference().child('friends');

    textUserName.text = widget.user.userName;
    textAddress.text = widget.user.address;
    textEmailAddress.text = widget.user.userEmail;

    print('uid profile ' + widget.uid + " iddd " + widget.user.uid);

    _userFriends.once().then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> values = snapshot.value;
      values.forEach((key, values) {
        if (widget.uid == values['senderId'].toString()) {
          if (widget.user.uid == values['receiverId'].toString()) {
            print('keyStatus ' + values['status']);
            if (values['status'] == 'pending') {
              setState(() {
                print('loader false 1');
                buttonName = "Cancel Request";
                isVisible = false;
              });
            } else {
              setState(() {
                print('loader false 2');
                buttonName = "Remove Friend";
                isVisible = false;
              });
            }
          } else {
            print('loader false 3');
            print('keyStatus else');
            setState(() {
              isVisible = false;
            });
          }
        } else {
          print('loader false 4');
          print('keyElse not found');
          //checkUserRequest(widget.user.id);
        }
      });
    });
  }

  void checkUserRequest(String userId) {
    _userFriends
        .orderByChild('senderId')
        .equalTo(userId)
        .once()
        .then((DataSnapshot snapshot) {
      if (snapshot != null && snapshot.value != null) {
        {
          Map<dynamic, dynamic> values = snapshot.value;
          values.forEach((key, values) {
            print('values ' + values['status']);
            if (values['status'] == 'Accepted') {
              setState(() {
                print('loader acceted');
                buttonName = "Remove Friend";
                isVisible = false;
              });
            } else if (values['status'] == 'Rejected') {
              setState(() {
                print('loader send request');
                buttonName = 'Send Request';
                isVisible = false;
              });
            } else {
              setState(() {
                print('loader accept');
                buttonName = "Accept Request";
                isVisible = false;
              });
            }
          });
          print('sender  match');
        }
      } else {
        print('sender  not match');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.user.userName),
        ),
        body: isVisible
            ? Center(
                child: Visibility(
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(Color(0xFFB2F2D52)),
                  ),
                  visible: isVisible,
                ),
              )
            : Form(
                key: _formKey,
                child: Container(
                  margin: EdgeInsets.only(left: 30, right: 30),
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 58,
                      ),
                      Hero(
                        tag: 'hero',
                        child: GestureDetector(
                          onTap: () {
                            getImageFromGallery();
                          },
                          child: CircleAvatar(
                              backgroundColor: Colors.transparent,
                              radius: 48.0,
                              child: widget.user.imageUrl.isEmpty
                                  ? Container(
                                      height: 90,
                                      width: 90,
                                      child: CircleAvatar(
                                        radius: 30.0,
                                        child: Text(widget.user.userName
                                            .substring(0, 1)),
                                        backgroundColor:
                                            const Color(0xFF20283e),
                                      ),
                                    )
                                  : Container(
                                      height: 90,
                                      width: 90,
                                      child: CircleAvatar(
                                        radius: 30,
                                        backgroundImage:
                                            NetworkImage(widget.user.imageUrl),
                                      ),
                                    )),
                        ),
                      ),
                      SizedBox(
                        height: 68,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.name,
                        autofocus: false,
                        controller: textUserName,
                        enabled: false,
                        decoration: InputDecoration(
                          hintText: 'FirstName',
                          contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0)),
                        ),
                        validator: (val) {
                          if (val.isEmpty)
                            return 'Please enter first name';
                          else
                            return null;
                        },
                        onSaved: (val) => _firstName = val,
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        autofocus: false,
                        controller: textEmailAddress,
                        enabled: false,
                        decoration: InputDecoration(
                          hintText: 'Email Address',
                          contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0)),
                        ),
                        validator: (val) {
                          if (val.isEmpty)
                            return 'Please enter email address';
                          else
                            return null;
                        },
                        onSaved: (val) => _email = val,
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.multiline,
                        textInputAction: TextInputAction.newline,
                        autofocus: false,
                        enabled: false,
                        maxLines: 3,
                        controller: textAddress,
                        maxLengthEnforced: true,
                        decoration: InputDecoration(
                          hintText: 'Address',
                          contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0)),
                        ),
                        validator: (val) {
                          if (val.isEmpty)
                            return 'Please enter address';
                          else
                            return null;
                        },
                        onSaved: (val) => _address = val,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 16.0),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24),
                          ),
                          onPressed: () async {
                            if (buttonName == 'Send Request') {
                              setState(() {
                                isVisible = true;
                              });
                              sendRequest(
                                  widget.uid, widget.user.uid, "pending");
                              print('check user');
                            } else if (buttonName == 'Cancel Request') {
                              {
                                Navigator.pop(context);
                                print('cancel request');
                              } //
                            } else if (buttonName == 'Accept Request') {
                              Fluttertoast.showToast(
                                  msg: 'Please check your request page');
                            } else {
                              Navigator.pop(context);
                              print('remove user');
                            }
                          },
                          padding: EdgeInsets.all(12),
                          color: Colors.lightBlueAccent,
                          child: Text(
                            buttonName,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ));
  }

  sendRequest(senderId, String receiverId, String status) async {
    final TransactionResult transactionResult =
        await _counterRef.runTransaction((MutableData mutableData) async {
      mutableData.value = (mutableData.value ?? 0) + 1;

      return mutableData;
    });

    if (transactionResult.committed) {
      _userFriends.push().set(<String, String>{
        "senderId": "" + senderId,
        "receiverId": "" + receiverId,
        "status": "" + status,
      }).then((_) {
        print('Transaction  committed.');
        print('frnd added');
        Fluttertoast.showToast(msg: 'Request send successfully');
        Navigator.of(context).pop();
      });
    } else {
      print('Transaction not committed.');
      if (transactionResult.error != null) {
        print(transactionResult.error.message);
      }
    }
  }
}
