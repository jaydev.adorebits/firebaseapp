import 'dart:async';
import 'dart:io';
import 'package:android_intent/android_intent.dart';
import 'package:firebase_app/constant/Constant.dart';
import 'package:firebase_app/screens/bottom_tab.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';

import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_app/model/user.dart';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:permission_handler/permission_handler.dart';

class RegisterUser extends StatefulWidget {
  String uid;
  String username;
  String emailAddress;

  RegisterUser({Key key, this.uid, this.username, this.emailAddress});

  @override
  RegisterUserState createState() => RegisterUserState();
}

class RegisterUserState extends State<RegisterUser> {
  String _firstName = "";
  String _email = "";
  String _address = "";
  String _latitude = "";
  String _longitude = "";
  String uid = "";
  String imageLink = "";
  String fToken = "";

  TextEditingController textUserName = TextEditingController();
  TextEditingController textEmailAddress = TextEditingController();
  TextEditingController textAddress = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  User user;

  bool isVisible = false;
  bool _anchorToBottom = false;

  //FirebaseDatabaseUtil databaseUtil;
  DatabaseReference _counterRef;
  DatabaseReference _userRef;
  DatabaseReference _userFriends;

  FirebaseDatabase database = new FirebaseDatabase();

  FirebaseStorage storage = FirebaseStorage.instance;
  StorageReference reference;

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  _getFirebaseToken() {
    _firebaseMessaging.getToken().then((token) => fToken = token);
    print('token : ' + fToken);
  }

  final PermissionHandler permissionHandler = PermissionHandler();
  Map<PermissionGroup, PermissionStatus> permissions;

  File imageURI;

  Future getImageFromCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      imageURI = image;
    });
  }

  Future getImageFromGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      imageURI = image;
    });
  }

  @override
  void initState() {
    super.initState();
    _getFirebaseToken();
    _counterRef = FirebaseDatabase.instance.reference().child('counter');
    _userRef = database.reference().child('user');
    //_userFriends = database.reference().child('friends');

    textUserName.text = widget.username;
    textEmailAddress.text = widget.emailAddress;
    uid = widget.uid;

//    _getLocation();
    requestLocationPermission();
    _gpsService();
  }

  @override
  void dispose() {
    super.dispose();
    //databaseUtil.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: isVisible
            ? Center(
                child: Visibility(
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(Color(0xFFB2F2D52)),
                  ),
                  visible: isVisible,
                ),
              )
            : Form(
                key: _formKey,
                child: Container(
                  margin: EdgeInsets.only(left: 30, right: 30),
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 68,
                      ),
                      Hero(
                          tag: 'hero',
                          child: GestureDetector(
                            onTap: () {
                              getImageFromGallery();
                            },
                            child: CircleAvatar(
                                backgroundColor: Colors.transparent,
                                radius: 48.0,
                                child: imageURI == null
                                    ? Image.asset('assets/images/bg.png')
                                    : CircleAvatar(
                                        backgroundColor: Colors.transparent,
                                        radius: 50.0,
                                        child: Image.file(imageURI,
                                            fit: BoxFit.cover),
                                      )),
                          )),
                      SizedBox(
                        height: 68,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.name,
                        autofocus: false,
                        controller: textUserName,
                        enabled: false,
                        decoration: InputDecoration(
                          hintText: 'FirstName',
                          contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0)),
                        ),
                        validator: (val) {
                          if (val.isEmpty)
                            return 'Please enter first name';
                          else
                            return null;
                        },
                        onSaved: (val) => _firstName = val,
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        autofocus: false,
                        controller: textEmailAddress,
                        enabled: false,
                        decoration: InputDecoration(
                          hintText: 'Email Address',
                          contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0)),
                        ),
                        validator: (val) {
                          if (val.isEmpty)
                            return 'Please enter email address';
                          else
                            return null;
                        },
                        onSaved: (val) => _email = val,
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      TextFormField(
                        keyboardType: TextInputType.multiline,
                        textInputAction: TextInputAction.newline,
                        autofocus: false,
                        maxLines: 6,
                        controller: textAddress,
                        maxLengthEnforced: true,
                        decoration: InputDecoration(
                          hintText: 'Address',
                          contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0)),
                        ),
                        validator: (val) {
                          if (val.isEmpty)
                            return 'Please enter address';
                          else
                            return null;
                        },
                        onSaved: (val) => _address = val,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 16.0),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24),
                          ),
                          onPressed: () async {
                            check().then((internet) {
                              if (internet != null && internet) {
                                if (imageURI != null) {
                                  if (_formKey.currentState.validate()) {
                                    _formKey.currentState.save();

                                    setState(() {
                                      isVisible = true;
                                    });
                                    uploadImage();
                                  } else
                                    print('data : null');
                                } else {
                                  Fluttertoast.showToast(
                                      msg: 'Select profile image');
                                }
                              } else {
                                Fluttertoast.showToast(
                                    msg:
                                        'Please check your internet connection');
                              }
                            });
                          },
                          padding: EdgeInsets.all(12),
                          color: Colors.lightBlueAccent,
                          child: Text(
                            'Register',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ));
  }

  User getData(bool isEdit) {
    return new User("", uid, _firstName, _email, _address, _latitude,
        _longitude, imageLink, "send", "0", fToken);
  }

  void uploadImage() async {
    reference = storage.ref().child("profileImages/" +
        imageURI.lastModifiedSync().millisecondsSinceEpoch.toString());
    StorageUploadTask uploadTask = reference.putFile(imageURI);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    imageLink = await taskSnapshot.ref.getDownloadURL();
    print('imageLink ' + imageLink + " ll");
    Timer(Duration(seconds: 5), () {
      registerUser(getData(false));
    });
    //registerUser(getData(false));
  }

  registerUser(User user) async {
    final TransactionResult transactionResult =
        await _counterRef.runTransaction((MutableData mutableData) async {
      mutableData.value = (mutableData.value ?? 0) + 1;

      return mutableData;
    });

    if (transactionResult.committed) {
      _userRef.child(user.uid).set(<String, String>{
        "uid": "" + user.uid,
        "userName": "" + user.userName,
        "userEmail": "" + user.userEmail,
        "address": "" + user.address,
        "latitude": "" + user.latitude,
        "longitude": "" + user.longitude,
        "imageUrl": "" + user.imageUrl,
        "friendStatus": "" + user.friendStatus,
        "friendId": "" + user.friendId,
        "token": "" + user.token,
      }).then((_) {
        print('Transaction  committed.');
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) => BottomTab(
                      userName: widget.username,
                      userEmail: widget.emailAddress,
                      uid: uid,
                    )),
            (Route<dynamic> route) => false);
      });
    } else {
      print('Transaction not committed.');
      if (transactionResult.error != null) {
        print(transactionResult.error.message);
      }
    }
  }

  final profile = Hero(
    tag: 'hero',
    child: CircleAvatar(
      backgroundColor: Colors.transparent,
      radius: 48.0,
      child: Image.asset('assets/images/bg.png'),
    ),
  );

  final firstName = TextFormField(
    keyboardType: TextInputType.name,
    autofocus: false,
    decoration: InputDecoration(
      hintText: 'FirstName',
      contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
    ),
    validator: (val) {
      if (val.isEmpty)
        return 'Please enter user name';
      else
        return null;
    },
    //onSaved: (val) => _firstName = val,
  );

  final lastName = TextFormField(
    keyboardType: TextInputType.name,
    autofocus: false,
    decoration: InputDecoration(
      hintText: 'LastName',
      contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
    ),
    validator: (val) {
      if (val.isEmpty)
        return 'Please enter first name';
      else
        return null;
    },
  );

  final registerButton = Padding(
    padding: EdgeInsets.symmetric(vertical: 16.0),
    child: RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(24),
      ),
      onPressed: () {
        //print('data : ' + _firstName + " " + _lastName);
      },
      padding: EdgeInsets.all(12),
      color: Colors.lightBlueAccent,
      child: Text(
        'Register',
        style: TextStyle(color: Colors.white),
      ),
    ),
  );

  _getLocation() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    debugPrint('location: ${position.latitude}');
    final coordinates = new Coordinates(position.latitude, position.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    print("${first.featureName} : ${first.addressLine}");
    setState(() {
      _latitude = position.latitude.toString();
      _longitude = position.longitude.toString();
      _address = first.addressLine;
      textAddress.text = _address;
      print("location :" +
          first.adminArea +
          " kk " +
          position.latitude.toString() +
          " k " +
          position.longitude.toString() +
          " ty");
    });

    /*
    final PermissionHandler _permissionHandler =
                            PermissionHandler();
                            var result = await _permissionHandler.requestPermissions([PermissionGroup.location]);

                            switch (result[PermissionGroup.location]) {
                              case PermissionStatus.granted:
                              // do something
                                print('check 1');
                                break;
                              case PermissionStatus.denied:
                              // do something
                                print('check 2');
                                break;
                              case PermissionStatus.disabled:
                              // do something
                                print('check 3');
                                break;
                              case PermissionStatus.restricted:
                              // do something
                                print('check 4');
                                break;
                              default:
     */
  }

  Future<bool> _requestPermission(PermissionGroup permission) async {
    final PermissionHandler _permissionHandler = PermissionHandler();
    var result = await _permissionHandler.requestPermissions([permission]);
    if (result[permission] == PermissionStatus.granted) {
      print('perm granted');
      return true;
    }
    return false;
  }

/*Checking if your App has been Given Permission*/
  Future<bool> requestLocationPermission({Function onPermissionDenied}) async {
    var granted = await _requestPermission(PermissionGroup.location);
    if (granted != true) {
      requestLocationPermission();
    }
    debugPrint('requestContactsPermission $granted');
    return granted;
  }

/*Show dialog if GPS not enabled and open settings location*/
  Future _checkGps() async {
    if (!(await Geolocator().isLocationServiceEnabled())) {
      if (Theme.of(context).platform == TargetPlatform.android) {
        showDialog(
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Can't get current location"),
                content:
                    const Text('Please make sure you enable GPS and try again'),
                actions: <Widget>[
                  FlatButton(
                      child: Text('Ok'),
                      onPressed: () {
                        final AndroidIntent intent = AndroidIntent(
                            action:
                                'android.settings.LOCATION_SOURCE_SETTINGS');
                        intent.launch();
                        Navigator.of(context, rootNavigator: true).pop();
                        _gpsService();
                      })
                ],
              );
            });
      }
    }
  }

/*Check if gps service is enabled or not*/
  Future _gpsService() async {
    if (!(await Geolocator().isLocationServiceEnabled())) {
      _checkGps();
      return null;
    } else {
      _getLocation();
      print('perm granted gps 451');
      return true;
    }
  }
}
