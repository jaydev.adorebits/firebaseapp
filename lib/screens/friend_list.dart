import 'dart:async';

import 'package:firebase_app/constant/Constant.dart';
import 'package:firebase_app/model/request_list_model.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class FriendList extends StatefulWidget {
  String userId;

  FriendList({Key key, this.userId});

  @override
  FriendListState createState() => FriendListState();
}

class FriendListState extends State<FriendList> {
  DatabaseReference _counterRef;
  DatabaseReference _userFriends;
  DatabaseReference _userRef;

  List<RequestListModel> users = List();
  List<RequestListModel> usersList = List();

  bool isVisible = true;
  bool flag = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Friend List'),
      ),
      body: isVisible
          ? Center(
              child: Visibility(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Color(0xFFB2F2D52)),
                ),
                visible: isVisible,
              ),
            )
          : Container(
              child: users.length != 0
                  ? ListView.builder(
                      itemCount: users.length,
                      itemBuilder: (BuildContext context, int index)
                      {
                        RequestListModel user = users[index];
                        return cardWidget(
                            users[index].imageUrl,
                            users[index].userName,
                            users[index].userEmail,
                            users[index].id,
                            user);
                      },
                    )
                  : Center(
                      child: Text('No friend found'),
                    )),
    );
  }

  @override
  void initState() {
    super.initState();

    check().then((intenet) {
      if (intenet != null && intenet) {
        print('net is available ');
        print('userId ' + widget.userId);

        FirebaseDatabase database = FirebaseDatabase.instance;
        _counterRef = FirebaseDatabase.instance.reference().child('counter');
        _userFriends = database.reference().child('friends');
        _userRef = database.reference().child('user');

        _userFriends
            .orderByChild('receiverId')
            .equalTo(widget.userId)
            .once()
            .then((DataSnapshot snapshot) {
          if (snapshot != null && snapshot.value != null) {
            Map<dynamic, dynamic> values = snapshot.value;
            values.forEach((key, values) {
              print('userFF ' + key + " kkk");

              if (values['status'].toString() == 'Accepted') {
                String senderId = values['senderId'].toString();
                String receiverId = values['receiverId'].toString();
                String status = values['status'].toString();
                print("index====<>" + senderId);

                getSingleUser(values['senderId'].toString(), key, senderId,
                    receiverId, status);
              } else {
                setState(() {
                  isVisible = false;
                });
                print('status accepted');
              }
            });
          } else {
            print('user ' + snapshot.key + " not kk");
            setState(() {
              isVisible = false;
            });
          }
        });
      } else
        Fluttertoast.showToast(msg: 'Please check your internet connection');
      // No-Internet Case
    });
  }

  Future getSingleUser(String senderKey, String updateKey, String senderId,
      String receiverId, String status) {
    try {
      _userRef.once().then((DataSnapshot snapshot) {
        if (snapshot != null && snapshot.value != null) {
          Map<dynamic, dynamic> values = snapshot.value;
          values.forEach((key, values) {
            print('key ' + key + " " + senderKey + " sender");
            if (key == senderKey) {
              String id = updateKey;
              String userName = values['userName'].toString();
              String userEmail = values['userEmail'].toString();
              String address = values['address'].toString();
              String latitude = values['latitude'].toString();
              String longitude = values['longitude'].toString();
              String imageUrl = values['imageUrl'].toString();
              String uid = values['uid'].toString();
              String token = values['token'].toString();
              print('userKS ' + values['userName'].toString() + " values  kk");
              print('userSize ' + users.length.toString() + " kk");
              users.add(new RequestListModel(
                  id,
                  uid,
                  userName,
                  userEmail,
                  address,
                  latitude,
                  longitude,
                  imageUrl,
                  senderId,
                  receiverId,
                  status,
                  token));

              setState(() {
                isVisible = false;
              });
            } else
              print('userKeeyyElse ' +
                  values['userName'].toString() +
                  " values  kk");
          });
        } else
          print('else ');
      });
    } catch (e) {
      print("exe exe" + e.toString());
    }

    Timer(Duration(seconds: 3), () {
      setState(() {
        for (int i = 0; i < users.length; i++) {
          for (int j = 0; j < users.length; j++) {
            if (users[i].uid == users[j].uid) {
              users.remove(j);
              print('userList ' + users.length.toString() + " kk");
            }
          }
        }
      });
    });
  }

  Widget cardWidget(String imageUrl, String userName, String userEmail,
      String id, RequestListModel user) {
    return Card(
      elevation: 2,
      margin: EdgeInsets.fromLTRB(14, 6, 14, 4),
      child: new Container(
          margin: EdgeInsets.all(8),
          child: new Center(
            child: Column(
              children: [
                new Row(
                  children: <Widget>[
                    Container(
                        height: 58.0,
                        width: 58.0,
                        child: imageUrl.isEmpty
                            ? CircleAvatar(
                                radius: 30.0,
                                child: Text(userName.substring(0, 1)),
                                backgroundColor: const Color(0xFF20283e),
                              )
                            : CircleAvatar(
                                radius: 30,
                                backgroundImage: NetworkImage(imageUrl),
                              )),
                    new Expanded(
                      child: new Padding(
                        padding: EdgeInsets.all(18.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text(
                              userName,
                              // set some style to text
                              style: new TextStyle(
                                  fontSize: 16.0, color: Colors.blue),
                            ),
                            new Text(
                              userEmail,
                              style: new TextStyle(
                                  fontSize: 16.0, color: Colors.blue),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
          padding: const EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)),
    );
  }
}
