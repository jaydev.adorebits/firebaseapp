import 'dart:async';

import 'package:firebase_app/screens/bottom_tab.dart';
import 'package:firebase_app/screens/home_screen.dart';
import 'package:firebase_app/screens/register_user_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

String name;
String email;
String imageUrl;
String uid;
final FirebaseAuth auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();

class AuthScreen extends StatefulWidget {
  @override
  AuthScreenState createState() => AuthScreenState();
}

class AuthScreenState extends State<AuthScreen> {
  bool isVisible = false;

  Future<FirebaseUser> _signIn() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication gsa =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: gsa.idToken, accessToken: gsa.accessToken);
    final AuthResult authResult = await auth.signInWithCredential(credential);
    final FirebaseUser firebaseUser = authResult.user;
    name = firebaseUser.displayName;
    email = firebaseUser.email;
    imageUrl = firebaseUser.photoUrl;
    uid = firebaseUser.uid;
    final FirebaseUser currentUser = await auth.currentUser();
    assert(firebaseUser.uid == currentUser.uid);
    return firebaseUser;
  }

  @override
  Widget build(BuildContext context) {
    var sWidht = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/bg.png"),
                    fit: BoxFit.cover)),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Visibility(
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(Color(0xFFB2F2D52)),
                  ),
                  visible: isVisible,
                ),
              )
            ],
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 60),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                height: 54.0,
                width: sWidht / 1.45,
                child: RaisedButton(
                  onPressed: () {
                    setState(() {
                      this.isVisible = true;
                    });
                    _signIn().whenComplete(() {
                      /*Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (context) => RegisterUser(uid:uid,username: name,emailAddress: email,)),
                              (Route<dynamic> route) => false);*/
                      Timer(
                          Duration(seconds: 3),
                          () => FirebaseDatabase.instance
                                  .reference()
                                  .child("user")
                                  .orderByChild("uid")
                                  .equalTo(uid)
                                  .once()
                                  .then((DataSnapshot snapshot) {
                                if (snapshot != null &&
                                    snapshot.value != null) {
                                  print('not null getting value');
                                  Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (context) => BottomTab(
                                                userName: name,
                                                userEmail: email,
                                                uid: uid,
                                              )),
                                      (Route<dynamic> route) => false);
                                } else {
                                  Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (context) => RegisterUser(
                                                uid: uid,
                                                username: name,
                                                emailAddress: email,
                                              )),
                                      (Route<dynamic> route) => false);
                                }
                              }));
                    }).catchError((onError) {
                      Navigator.pushReplacementNamed(context, "/auth");
                    });
                  },
                  child: Text(
                    'Continue With Google',
                    style: TextStyle(fontSize: 16),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  elevation: 5,
                  color: Color(0XFFF7C88C),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
