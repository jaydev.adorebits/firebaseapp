import 'package:firebase_app/firebase/push_notification.dart';
import 'package:firebase_app/screens/auth_screen.dart';
import 'package:firebase_app/screens/bottom_tab.dart';
import 'package:firebase_app/screens/home_screen.dart';
import 'package:firebase_app/screens/register_user_screen.dart';
import 'package:firebase_app/screens/splash_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

var routes = <String, WidgetBuilder>{
  "/auth": (BuildContext context) => AuthScreen(),
  "/home": (BuildContext context) => HomeScreen(),
};

void main() {
  runApp(MyApp());
}
/*void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Google SignIn',
    routes: routes,
    home: SplashScreen(),
  ));
}*/

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  String _message = "";
  String _body = "";

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    getMessage();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Google SignIn',
      routes: routes,
      home: SplashScreen(),
    );
  }

  void getMessage() {
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
      print('on message $message' + " main");

      setState(() {
        _message = message["notification"]["title"];
        _body = message["notification"]["body"];
        print('on message msg1 ' + _message);
        print('on message body1 ' + _body + " kk " + message["notification"]["body"]);
      });
    }, onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
      setState(() {
        _message = message["notification"]["title"];
      });
    }, onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
      setState(() {
        _message = message["notification"]["title"];
      });
    });
  }
}

// google sign in , first var user profile fill up fnm,lnm,location,
// list of User
// auto location run
// tab search 1 km sign up location latlong  req= signup  near location signup ahem 5 km radius location current location
// search location 5 km list near list current
// RND 4 o clock
// request send in user list :
// bdha user frnds accept and req list accept request
/*
frnd table
- user
    - user list
    -
 */
// status-> pending,accepted,rejected
// block and provider
// stream controller
